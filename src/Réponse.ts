import { QuestionInterface } from "./Question";

export interface RéponseInterface{
    text: string;
    next : number

}

export class Réponse {
    constructor(text: RéponseInterface){
        this.text = text.text
        this.next = text.next
    }
    public text: string;
    public next: number
}
