import { JsonParser } from "./JsonParser";
import { Question } from "./Question";

export default class IEventParser extends JsonParser{
    Parse(){
        return this.GetJson() as Array<Question>
    }
}