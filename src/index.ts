import IEventParser from "./IEventParser";
import { Question, QuestionInterface } from "./Question"

let elem = document.getElementById("app")

let data: Question[] = new IEventParser().Parse();

function getQuestionById(id: number): Question{
    let found = data.filter((q: Question) => q.id === id);
    return found[0]
}

displayQuestion(getQuestionById(1))

function displayQuestion(quest: Question){
    // Vide la div
    elem.innerHTML = "";

    // Ajout question
    let question = document.createElement("h3")
    question.innerText = quest.text
    elem.appendChild(question)

    elem.innerHTML += "<br />"

    // Ajout réponses
    for (const réponse of quest.choices) {
            let btn = document.createElement("button")
            btn.type = "button";
            btn.innerText = réponse.text;
            btn.addEventListener("click", (ev: MouseEvent) => { 
                let nextQuestion = getQuestionById(réponse.next)
                console.log(nextQuestion)
                if (nextQuestion.choices) displayQuestion(nextQuestion)
                else displayEnd(nextQuestion.text)
            })
            elem.appendChild(btn)
    }
}

function displayEnd(gameOverMessage: string){
    elem.innerHTML = "";

    // Ajout mot de fin
    let question = document.createElement("h2")
    question.innerText = gameOverMessage

    elem.appendChild(question);

    let btn = document.createElement("button")
    btn.type = "button";
    btn.innerText = "Recommencer";
    btn.addEventListener("click", (ev: MouseEvent) => { 
        // displayQuestion(début as QuestionInterface)
    })
    elem.appendChild(btn)
}