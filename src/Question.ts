import { Réponse, RéponseInterface } from "./Réponse";

export interface QuestionInterface{
    id: number;
    text: string;
    choices?: Array<RéponseInterface>
}

export class Question {
    constructor(question: QuestionInterface){
        this.id = question.id;
        this.text = question.text;
        this.choices = question.choices;

    }
    public id: number;
    public text : string
    public choices?: Array<Réponse>
}

